package luxor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

//Maga a program a mainMeni statikus f�ggv�nyben fog futni, a Menu oszt�lyon bel�l,
//aminek csak statikus met�dusai vannak
public class Menu {
	
	private static Luxor luxorGame;
	
	//A f�men� f�ggv�ny, innen �rhet� el az alkalmaz�s valamennyi funkci�ja
	static public int mainMenu() throws NumberFormatException {
		//TUDO: Ezzen m�g kezdeni k�ne valami, vagy void
		int returnCode = 0;
		//Az aktu�lis men� funkci� kezel�s�heu
		int loop = 0;
		try (BufferedReader bufferedReader =
				new BufferedReader(new InputStreamReader(System.in))) {
			while(loop == 0) {
				try {
					System.out.println(Texts.WELCOMETEXT);
					System.out.println(Texts.GAME);
					System.out.println(Texts.WINNINGNUMBERS);
					System.out.println(Texts.SETTINGS);
					System.out.println(Texts.EXIT);
					loop = Integer.parseInt(bufferedReader.readLine());
					switch(loop) {
					case 1: loop = InitLuxorGame(); break;
					}
					
				}
				catch (LuxorGameException e) {
					e.printStackTrace();
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return returnCode;
	}
	
	static private int InitLuxorGame() throws LuxorGameException {
		int loop = 0;
		//J�NOS: Ezt a readert nem rakhatom be a try() blokj�ban, k�l�nben
		//az �sszes inputStreamet bez�rja, �s a men� IOException dob azzal,
		//hogy Stream closed.
		BufferedReader bufferedReader =
				new BufferedReader(new InputStreamReader(System.in));
		try {
			while(loop == 0) {
				System.out.println(Texts.BYHAND);
				System.out.println(Texts.FROMFILE);
				loop = Integer.parseInt(bufferedReader.readLine());
				if(loop == 1) {
					try (BufferedReader inputFromConsole = 
							new BufferedReader(new InputStreamReader(System.in))) {
						luxorGame = new Luxor(inputFromConsole);
						
					}
					catch(IOException e) {
						e.printStackTrace();
					}
				}
				else if(loop == 2) {
					luxorGame = new Luxor();
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return 0;
		
	}

}
