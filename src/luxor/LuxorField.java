/**
 * 
 */
package luxor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.stream.Stream;

/**
 * @author Skriba
 *
 */


public class LuxorField {
	
	static final int MAXNUMBERINFIELD = 25;

	//T�rolja a mez� sz�mait
	private ArrayList<LuxorFieldNumber> fieldNumbers;
	//Jelzi, hogy nyertes-e a mez�ben l�v� keret
	private boolean frameWin;
	//Jelzi, hogy nyertes-e a mez�ben l�v� k�p
	private boolean pictureWin;
	//Jelzi, hogy nyertes-e a mez�
	private boolean fieldWin;
	
	LuxorField(ArrayList<Integer> inputList) {
		//Ezt a konstruktort megh�v� f�ggv�ny garant�lja, hogy inputList-nek legfeljebb
		//25 eleme lesz, ez�rt itt csak a biztons�g kedv��rt ker�l ellen�rz�sre
		fieldNumbers = new ArrayList<LuxorFieldNumber>();
		frameWin = false;
		pictureWin = false;
		fieldWin = false;
		if(inputList.size() < 25 || inputList.size() > 25)
			throw new IllegalArgumentException("Illegal field size");
		for(int i = 0; i < 25; ++i)
			fieldNumbers.add(new LuxorFieldNumber(inputList.get(i), i));
	}
	
	//Ki�rja a mez�ben lev� sz�mokat
	public void printField() {
		for(int i = 0; i < MAXNUMBERINFIELD; ++i) {
			System.out.print(String.format("%1$2s", fieldNumbers.get(i).number));
			if((i+1)%5 == 0)
				System.out.print("\n");
			else
				System.out.print(" ");
		}
	}
	
	
}
