/**
 * 
 */
package luxor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.stream.Stream;

/**
 * @author Skriba
 *
 */
public class Luxor {
	
	//TUDO: ezt bajd a be�ll�t�sokb�l fogja kiolvani a program
	

	//A megj�tszott mez�ket t�rolja
	private ArrayList<LuxorField> fields;
	//A nyertes sz�mokat t�rolja
	private ArrayList<Integer> winningNumbers;
	
	//A program alap�rtelmez�sben a "luxor.txt" nev� fileb�l olvassa be a mez�ket
	public Luxor() throws FileNotFoundException, LuxorGameException {
		fields = new ArrayList<LuxorField>();
		Path luxorInputFilePath = Paths.get(Settings.LUXORINPUTFILEPATH);
		try (Stream<String> lines = Files.lines(luxorInputFilePath)) {
			if(!Files.exists(luxorInputFilePath)) {
				Files.createFile(luxorInputFilePath);
				//Ezen a ponton a bemeneti file biztos �res, ezt jelezni kell
				throw new FileNotFoundException("File created");
			}
			String inputString = new String(Files.readAllBytes(luxorInputFilePath));
			InputToFieldNumbers(inputString);
		}
		catch(FileNotFoundException e) {
			if(e.getMessage().equals("File created"))
				//A file nem l�tez�s�b�l keletkez� hiba a men�ben lesz lekezelve
				throw e;
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public Luxor(BufferedReader inputFromConsole) {
		
	}
	//A beolvasott sorokat tokeniz�lja, majd a tokenekb�l kiolvassa a sz�mokat
	//TUDO: Meg kell m�g �rni, hogy sz�molja a l�her�ket
	private void InputToFieldNumbers(String inputString) throws LuxorGameException {
		StringTokenizer tokenizedInput = new StringTokenizer(inputString, Settings.SEPCHAR + "\r\n");
		//Lek�rdezi a string tokenek sz�m�t, cs�kkentve ettel a k�s�bbi f�ggv�nyh�v�sok sz�m�t
		int tokensNumber = tokenizedInput.countTokens();
		//Egy �rv�nyes luxor j�t�k legal�bb 50 karaktert tartalmaz, ha enn�l kevesebb van,
		//hib�s a bemener
		if(tokensNumber < 50)
			throw new LuxorGameException("Invalid luxor game", LuxorExceptionType.GAME);
		//Ha egy luxor mez� 25 karakter tartalmaz, ha a beolvasott sz�mok databsz�ma nem 25
		//t�bbsz�r�se, akkor egy mez�re t�bb vagy kevesebb sz�m jut az �rv�nyesn�l
		if(tokensNumber%25 != 0)
			throw new LuxorGameException("Invalid luxor field", LuxorExceptionType.FIELD);
		
		ArrayList<Integer> inputNumbers = new ArrayList<Integer>();
		int nextNumber;
		//Ameddig m�g van karakter a Stringben, addig 25-s�vel felt�lt egy seg�d list�t, amivel
		//l�trehozza a mez�ket. Minden sz�mn�l ellen�rzi az �rv�nyess�g�t is
		while(tokenizedInput.hasMoreTokens()) {
			for(int i = 0; i < 25; ++i) {
				nextNumber = Integer.parseInt(tokenizedInput.nextToken());
				if(nextNumber >= 0 && nextNumber <= 75)
					inputNumbers.add(nextNumber);
				else
					throw new LuxorGameException("Invalid input number", LuxorExceptionType.NUMBER);
			}
			fields.add(new LuxorField(inputNumbers));
			inputNumbers.clear();
		}
		
	}
	
	//Ki�rja a megj�tszott sz�mokat
	public void printGame() {
		int numberOfFields = fields.size();
		for(int i = 0; i < numberOfFields; ++i) {
			fields.get(i).printField();
			System.out.print("\n");
		}
	}
	
}
