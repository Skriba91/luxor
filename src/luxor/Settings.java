/**
 * 
 */
package luxor;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * @author Skriba
 *
 */

//Be�ll�t�sokat kezel� oszt�ly. Csak statikus met�dusokkal �s tagv�ltoz�kkal rendelkezik.
public class Settings {
	
	private static final String SETTINGSFILEPATH = 
			"D:\\DANI\\OneDrive\\DANI\\Programozas\\Java\\Luxor\\src\\luxor\\settings.lrs";
	
	private static final String DEFAULTLUXORINPUTFILENAME = 
			"D:\\DANI\\OneDrive\\DANI\\Programozas\\Java\\Luxor\\src\\luxor\\luxor.txt";

	private static final String SEPARATION_CHARACTER = "SEPCHAR";
	
	private static final String FILE_PATH = "FILEPATH";
	
	public static String SEPCHAR;
	public static String LUXORINPUTFILEPATH;
	
	public static void loadSettings() {
		Properties settings = new Properties();
		Path settingsFilePath = Paths.get(SETTINGSFILEPATH);
		try {
			if(!Files.exists(settingsFilePath))
				loadDefaultSettings(settingsFilePath, settings);
			settings.load(Files.newInputStream(settingsFilePath));
			SEPCHAR = settings.getProperty(SEPARATION_CHARACTER);
			LUXORINPUTFILEPATH = settings.getProperty(FILE_PATH);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void loadDefaultSettings(Path settingsFilePath, Properties settings)
			throws IOException {
		Files.createFile(settingsFilePath);
		SEPCHAR = " ";
		LUXORINPUTFILEPATH = DEFAULTLUXORINPUTFILENAME;
		settings.setProperty(SEPARATION_CHARACTER, SEPCHAR);
		settings.setProperty(FILE_PATH, LUXORINPUTFILEPATH);
		settings.store(Files.newOutputStream(settingsFilePath), "Luxor settings");
	}

}
