/**
 * 
 */
package luxor;

/**
 * @author Skriba
 *
 */
public class Texts {
	
	public static final String WELCOMETEXT = new String("�dv�z�llek a luxor szerv�ny ellen�rz� programban. "
			+ "Navig�ni a men� elemek sz�m�nak be�r�s�val tudsz.");
	public static final String GAME = new String("1 Saj�t szelv�ny megad�sa");
	public static final String WINNINGNUMBERS = new String("2 Nyer�sz�mok megad�sa");
	public static final String SETTINGS = new String("3 Be�ll�t�sok");
	public static final  String EXIT = new String("4 Kil�p�s");
	public static final String BYHAND = new String("1 Szelv�ny k�zi megad�sa konzolon kereszt�l");
	public static final String FROMFILE = new String("2 Szelv�ny beolvas�sa fileb�l");

}
