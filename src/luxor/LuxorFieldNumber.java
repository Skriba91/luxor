/**
 * 
 */
package luxor;

/**
 * @author Skriba
 *
 */

enum Position {
	FRAME, PICTURE;
}

//A luxornak megfelel� sz�mokat t�rol, �s jelzi, hogy a sz�m nyertes-e, illetve a hogy a keret
//vagy a k�p r�sze
public class LuxorFieldNumber {
	
	//Minden tagv�ltoz� public, mert egy �tlagos (2 mez�s) szelv�ny eset�n csak a nyertes sz�mok ellen�rz�se
	//150 f�ggv�ny h�v�st jelentene
	
	//Luxor sz�m
	public int number;
	//Jelzi, hogy nyertes-e a sz�m
	public boolean winningNumber;
	//Jelzi, hogy a sz�m a k�p vagy a keret r�sze-e, az�rt final, mert egy sz�m nem v�lthat soha poz�ci�t
	public final Position position;

	//Konstruktor arra az esetre, ha m�g nem imserj�k a nyer�sz�mokat
	//�s kisz�m�tja a sz�m poz�ci�j�t
	public LuxorFieldNumber(int number, int numberPos) throws IllegalArgumentException {
			setNumber(number);
			winningNumber = false;
			this.position = getPosition(numberPos);
		}
	
	//Konstruktor arra az esetre, ha m�g nem imserj�k a nyer�sz�mokat
	public LuxorFieldNumber(int number, Position position) throws IllegalArgumentException {
		setNumber(number);
		winningNumber = false;
		this.position = position;
	}
	
	//Konstruktor arra at esetre, ha m�r ismerj�k a nyer�sz�mokat
	public LuxorFieldNumber(int number, boolean isWinning, Position position) throws IllegalArgumentException {
		setNumber(number);
		winningNumber = isWinning;
		this.position = position;
	}
	
	//A number v�ltoz�t �ll�tja be
	public void setNumber(int number) throws IllegalArgumentException {
		if(number >= 0 && number <= 75)
			this.number = number;
		else
			throw new IllegalArgumentException("Luxor �ltal nem haszn�lt sz�m");
	}
	
	//Visszat�r a number �rt�k�vel
	public int getNumber() {
		return number;
	}
	
	//Be�ll�tja hogy a sz�m nyer� sz�m-e
	public void setWinning(boolean isWinning) {
		winningNumber = isWinning;
	}
	
	//Visszat�r azzal, hogy az adott sz�m nyer�-e
	public boolean getWinning() {
		return winningNumber;
	}
	
	//Visszat�r a sz�m poz�ci�j�val
	public Position getPosition() {
		return position;
	}
	
	public Position getPosition(int intPos) {
		if(intPos < 7 || intPos > 19)
			return Position.FRAME;
		else if(intPos%5 == 0 || intPos == 11 || intPos == 16)
			return Position.FRAME;
		else
			return Position.PICTURE;
	}

}
