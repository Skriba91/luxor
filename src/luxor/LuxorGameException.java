package luxor;

enum LuxorExceptionType {
	GAME, FIELD, EMPTYFILE, CLOVER, NUMBER
}

//Ez a kiv�teloszt�ly a luxor j�t�kkal kapcsolatos kiv�telek jelz�s�re szolg�ll
//A f�bb hibat�pusok, ha nincs legal�bb k�t mez� (legal�bbb 150 karakter)
//vagy egy mez�ben nincs elegend� karakter (25), vagy nincs meg az �t darab
//l�here.

@SuppressWarnings("serial")
public class LuxorGameException extends Exception {
	
	public LuxorExceptionType type;
	
	public LuxorGameException(String message) {
		super(message);
	}
	
	public LuxorGameException(LuxorExceptionType type) {
		this.type = type;
	}
	
	public LuxorGameException(String message, LuxorExceptionType type) {
		super(message);
		this.type = type;
	}

}
